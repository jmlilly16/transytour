//
//  ViewController.swift
//  TransyTour
//
//  Created by Skyler Moran on 2/2/16.
//  Copyright © 2016 Skyler Moran and Mason Lilly. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldAutorotate() -> Bool {
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
        {
            return false
        }
        
        if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
        {
            return true
        }
        return true
    }
    
    @IBAction func unwindToLogout(sender: UIStoryboardSegue)
    {
        _ = sender.sourceViewController
    }
    
    //MARK: Properties
    

}

