//
//  MapNameViewController.swift
//  TransyTour
//
//  Created by Skyler Moran on 2/22/16.
//  Copyright © 2016 Skyler Moran and Mason Lilly. All rights reserved.
//

import UIKit

class MapNameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        MapName.transform = CGAffineTransformMakeRotation(3.14/2);

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldAutorotate() -> Bool {
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation))
        {
            return false
        }
        
        if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation))
        {
            return true
        }
        return true
    }
    
    
    
    //MARK: Properties

    @IBOutlet var MapName: UIView!
    

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
