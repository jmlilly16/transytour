//
//  HomeViewController.swift
//  TransyTour
//
//  Created by Skyler Moran on 2/9/16.
//  Copyright © 2016 Skyler Moran and Mason Lilly. All rights reserved.
//
//
import MapKit
import UIKit
import Foundation
import CoreLocation

class HomeViewController: UIViewController, CLLocationManagerDelegate, UIAlertViewDelegate{
    var locationManager: CLLocationManager!
    var currentExhibit = ""
    
    let bottomLeftCoordinate = CLLocationCoordinate2DMake(16.1, 37.1)
    let topRightCoordinate = CLLocationCoordinate2DMake(18.1, 39.1)
    
    let HHbottomLeftCoordinate = CLLocationCoordinate2DMake(38.051906, -84.493715)
    let HHtopRightCoordinate = CLLocationCoordinate2DMake(38.052423, -84.493546)
    
    let OMbottomLeftCoordinate = CLLocationCoordinate2DMake(38.051315, -84.493406)
    let OMtopRightCoordinate = CLLocationCoordinate2DMake(38.051935, -84.493313)
    
    let BSCbottomLeftCoordinate = CLLocationCoordinate2DMake(38.050454, -84.493375)
    let BSCtopRightCoordinate = CLLocationCoordinate2DMake(38.051075, -84.492873)
    
    let fieldBottomLeftCoordinate = CLLocationCoordinate2DMake(38.057386, -84.497460)
    let fieldTopRightCoordinate = CLLocationCoordinate2DMake(38.059544, -84.496624)
    
    let rosyBottomLeftCoordinate = CLLocationCoordinate2DMake(38.054279, -84.493995)
    let rosyTopRightCoordinate = CLLocationCoordinate2DMake(38.055003, -84.493723)
    
    let MFAbottomLeftCoordinate = CLLocationCoordinate2DMake(38.051904, -84.492755)
    let MFAtopRightCoordinate = CLLocationCoordinate2DMake(38.052662, -84.492624)
    let admitBottomLeftCoordinate = CLLocationCoordinate2DMake(38.051451, -84.494314)
    let admitTopRightCoordinate = CLLocationCoordinate2DMake(38.051814, -84.494091)
    
    let libBottomLeftCoordinate = CLLocationCoordinate2DMake(38.051698, -84.494533)
    let libTopRightCoordinate = CLLocationCoordinate2DMake(38.052323, -84.494351)
    
    let ForrBottomLeftCoordinate = CLLocationCoordinate2DMake(38.052972, -84.494449)
    let ForrTopRightCoordinate = CLLocationCoordinate2DMake(38.054273, -84.494299)
    
    let beckBottomLeftCoordinate = CLLocationCoordinate2DMake(38.052609, -84.493680)
    let beckTopRightCoordinate = CLLocationCoordinate2DMake(38.053344, -84.492802)
    
    let cowBottomLeftCoordinate = CLLocationCoordinate2DMake(38.050653, -84.493571)
    let cowTopRightCoordinate = CLLocationCoordinate2DMake(38.051112, -84.493425)
    
    let artBottomLeftCoordinate = CLLocationCoordinate2DMake(38.052539, -84.492182)
    let artTopRightCoordinate = CLLocationCoordinate2DMake(38.053059, -84.492271)
    
    let hallBottomLeftCoordinate = CLLocationCoordinate2DMake(38.049549, -84.492566)
    let hallTopRightCoordinate = CLLocationCoordinate2DMake(38.050998, -84.492409)
    
    let CarnBottomLeftCoordinate = CLLocationCoordinate2DMake(38.049809, -84.495898)
    let CarnTopRightCoordinate = CLLocationCoordinate2DMake(38.050303, -84.495410)
    
    let LittleBottomLeftCoordinate = CLLocationCoordinate2DMake(38.051583, -84.492512)
    let LittleTopRightCoordinate = CLLocationCoordinate2DMake(38.051936, -84.492142)
    
    let HazelBottomLeftCoordinate = CLLocationCoordinate2DMake(38.051036, -84.492498)
    let HazelTopRightCoordinate = CLLocationCoordinate2DMake(38.051454, -84.492295)
    
    let AlumBottomLeftCoordinate = CLLocationCoordinate2DMake(38.053859, -84.493323)
    let AlumTopRightCoordinate = CLLocationCoordinate2DMake(38.054362, -84.493103)
    
    let PooleBottomLeftCoordinate = CLLocationCoordinate2DMake(38.055144, -84.493865)
    let PooleTopRightCoordinate = CLLocationCoordinate2DMake(38.055537, -84.493420)
    
    var passTest = "TestT"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        
        //Call the URL of the exhibit list on the server
        if exhibits == nil
        {
            let URL = createExhibitListURL()
            //Download the exhibit list file
            getDataFromURL(URL)
            {(data,response,error) in
                dispatch_async(dispatch_get_main_queue())
                {()->Void in
                    var exhibitText: String?
                    if let data = data
                    {
                        exhibitText = String(data: data, encoding: NSUTF8StringEncoding)
                    }
                    if error != nil || exhibitText != nil && exhibitText?.rangeOfString("404 Not Found") != nil
                    {
                        NSLog("Failed to access exhibitList.txt")
                    }
                    else if let exhibitText = exhibitText
                    {
                        exhibits = parseExhibitFile(exhibitText)
                        for exhibit in exhibits!
                        {
                            let factURL = createFactListURL(exhibit.shortname)
                            getDataFromURL(factURL)
                            {(data,response,error) in
                                dispatch_async(dispatch_get_main_queue())
                                {
                                    var factText: String?
                                    if let data = data
                                    {
                                        factText = String(data: data, encoding: NSUTF8StringEncoding)
                                    }
                                    if error != nil || factText != nil && factText?.rangeOfString("404 Not Found") != nil
                                    {
                                        NSLog("Failed to access facts.txt for exhibit \(exhibit.shortname)")
                                    }
                                    else if let factText = factText
                                    {
                                        exhibit.parseFactFile(factText)
                                    }
                                }
                            }
                            let imageURL = createCaptionListURL(exhibit.shortname)
                            getDataFromURL(imageURL)
                            {(data,response,error) in
                                dispatch_async(dispatch_get_main_queue())
                                {
                                    var captionText: String?
                                    if let data = data
                                    {
                                        captionText = String(data: data, encoding: NSUTF8StringEncoding)
                                    }
                                    if error != nil || captionText != nil && captionText?.rangeOfString("404 Not Found") != nil
                                    {
                                        NSLog("Failed to access captions.txt for exhibit \(exhibit.shortname)")
                                    }
                                    else if let captionText = captionText
                                    {
                                        exhibit.parseCaptionFile(captionText)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        IntroPic.contentMode = .ScaleAspectFit
        
        let status = CLLocationManager.authorizationStatus()
        GoToSetting.hidden = true
        
        if(status == CLAuthorizationStatus.Denied){
            MoreInfo.hidden = true
            IntroPic.hidden = true
            helpLabel.hidden = true
            helpMeButton.hidden = true
            DebugCoor.hidden = true
            FMILabel.hidden = true
            GPSTest.hidden = true
            MenuButton.hidden = true
            SettingButton.hidden = true
            CameraButton.hidden = true
            PicInfo.hidden = true
            
            let myAlert = UIAlertView()
            myAlert.title = "Location Service Disabled"
            myAlert.message = "To use this app, please go to Settings and turn on Location Service."
            myAlert.addButtonWithTitle("OK")
            myAlert.delegate = self
            myAlert.show()
            
            GoToSetting.hidden = false
            
            
        }
        else{
            
            MoreInfo.hidden = true
            IntroPic.hidden = true
            helpLabel.hidden = true
            helpMeButton.hidden = true
            DebugCoor.hidden = true
            FMILabel.hidden = true
            GPSTest.hidden = false
            MenuButton.hidden = false
            SettingButton.hidden = false
            CameraButton.hidden = false
            GoToSetting.hidden = true
            PicInfo.hidden = false
            
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.dismissMenu))
        view.addGestureRecognizer(tap)
        
        LeftMenu.hidden = true
        RightMenu.hidden = true
        
        
        
        // Do any additional setup after loading the view.
    }
    
    //If you click outside of the Menu bars, they close
    func dismissMenu() {
        LeftMenu.hidden = true
        RightMenu.hidden = true
    }
    
    /*override func shouldAutorotate() -> Bool {
        return false
    }*/
    
    @IBAction func showAlert(sender: AnyObject) {
        let actionSheetController: UIAlertController = UIAlertController(title: "Confirm", message: "Do you want to go to the Settings?", preferredStyle: .Alert)
        
        let leaveAction: UIAlertAction = UIAlertAction(title: "Take Me There", style: .Default) { action -> Void in
            let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                UIApplication.sharedApplication().openURL(url)
                exit(0)
            }
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Default) { action -> Void in
        }
        
        actionSheetController.addAction(leaveAction)
        actionSheetController.addAction(cancelAction)
        
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toggleLeftMenu(sender: AnyObject) {
        toggleLeftMenu()
    }
    
    
    func toggleLeftMenu(){
        LeftMenu.hidden = false
        RightMenu.hidden = true
    }
    
    @IBAction func toggleRightMenu(sender: AnyObject) {
        toggleRightMenu()
    }
    
    func toggleRightMenu(){
        RightMenu.hidden = false
        LeftMenu.hidden = true
    }
    
    func flipLabels(){
        MoreInfo.hidden = false
        IntroPic.hidden = false
        helpLabel.hidden = true
        helpMeButton.hidden = true
        DebugCoor.hidden = true
        FMILabel.hidden = false
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        //NSLog("\(locValue)")
        
        GPSTest.text = "No Locations Nearby"
        passTest = "No Location Found"
        MoreInfo.hidden = true
        IntroPic.hidden = true
        helpLabel.hidden = false
        helpMeButton.hidden = false
        DebugCoor.hidden = true
        FMILabel.hidden = true
        
        if let exhibits = exhibits
        {
            for exhibit in exhibits
            {
                if exhibit.containsLocation(locValue)
                {
                    currentExhibit = exhibit.shortname
                    passTest = exhibit.fullname
                    GPSTest.text = exhibit.fullname
                    displayCoverImage(exhibit, display: IntroPic)
                    flipLabels()
                    break;
                }
            }
        }
        
        locationValues.text = ("locations = \(locValue.latitude) \(locValue.longitude)")
        /*
        //Testing Locations
        if ((locValue.latitude >= bottomLeftCoordinate.latitude && locValue.longitude >= bottomLeftCoordinate.longitude) && (locValue.latitude <= topRightCoordinate.latitude && locValue.longitude <= topRightCoordinate.longitude)){
            GPSTest.text = "Haupt"
            passTest = "Haupt"
            MoreInfo.hidden = false
        }
        if (locValue.latitude == 38 && locValue.longitude == 11){
            GPSTest.text = "Athletic Complex"
            passTest = "Athletic Complex"
            MoreInfo.hidden = false
        }
        
        //REAL Locations
        
        //38.052216, -84.493549 is in Haupt
        if ((locValue.latitude >= HHbottomLeftCoordinate.latitude && locValue.longitude >= HHbottomLeftCoordinate.longitude) && (locValue.latitude <= HHtopRightCoordinate.latitude && locValue.longitude <= HHtopRightCoordinate.longitude)){
            GPSTest.text = "Haupt"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Haupt"
            IntroPic.image = UIImage(named:"Haupt")
            flipLabels()
        }
        
        //38.052320, -84.492715 is center of MFA
        if ((locValue.latitude >= MFAbottomLeftCoordinate.latitude && locValue.longitude >= MFAbottomLeftCoordinate.longitude) && (locValue.latitude <= MFAtopRightCoordinate.latitude && locValue.longitude <= MFAtopRightCoordinate.longitude)){
            GPSTest.text = "MFA"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "MFA"
            IntroPic.image = UIImage(named:"mitchell")
            flipLabels()
        }
        
        //38.051675, -84.493368 is center of Old Morrison.
        if ((locValue.latitude >= OMbottomLeftCoordinate.latitude && locValue.longitude >= OMbottomLeftCoordinate.longitude) && (locValue.latitude <= OMtopRightCoordinate.latitude && locValue.longitude <= OMtopRightCoordinate.longitude)){
            GPSTest.text = "Old Morrison"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Old Morrison"
            flipLabels()
            IntroPic.image = UIImage(named:"OMpic")
            
        }
        
        //locValue.latitude == 38.050896 && locValue.longitude == -84.493111 is entrance to BSC
        if ((locValue.latitude >= BSCbottomLeftCoordinate.latitude && locValue.longitude >= BSCbottomLeftCoordinate.longitude) && (locValue.latitude <= BSCtopRightCoordinate.latitude && locValue.longitude <= BSCtopRightCoordinate.longitude)){
            GPSTest.text = "BSC"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "BSC"
            IntroPic.image = UIImage(named:"brown")
            flipLabels()
        }
        
        //38.058357, -84.496967 is center T
        if ((locValue.latitude >= fieldBottomLeftCoordinate.latitude && locValue.longitude >= fieldBottomLeftCoordinate.longitude) && (locValue.latitude <= fieldTopRightCoordinate.latitude && locValue.longitude <= fieldTopRightCoordinate.longitude)){
            GPSTest.text = "Field"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Field"
            IntroPic.image = UIImage(named:"athletics_complex")
            flipLabels()
        }
        
        //38.054659, -84.493728 is my room
        if ((locValue.latitude >= rosyBottomLeftCoordinate.latitude && locValue.longitude >= rosyBottomLeftCoordinate.longitude) && (locValue.latitude <= rosyTopRightCoordinate.latitude && locValue.longitude <= rosyTopRightCoordinate.longitude)){
            GPSTest.text = "Rosenthal"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Rosenthal"
            IntroPic.image = UIImage(named:"rosenthal")
            flipLabels()
        }
        
        //38.051653, -84.494214 is center
        if ((locValue.latitude >= admitBottomLeftCoordinate.latitude && locValue.longitude >= admitBottomLeftCoordinate.longitude) && (locValue.latitude <= admitTopRightCoordinate.latitude && locValue.longitude <= admitTopRightCoordinate.longitude)){
            GPSTest.text = "Admission/Jazzmans"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Admission/Jazzmans"
            IntroPic.image = UIImage(named:"glenn")
            flipLabels()
        }
        
        //38.051900, -84.494356 in lib
        if ((locValue.latitude >= libBottomLeftCoordinate.latitude && locValue.longitude >= libBottomLeftCoordinate.longitude) && (locValue.latitude <= libTopRightCoordinate.latitude && locValue.longitude <= libTopRightCoordinate.longitude)){
            GPSTest.text = "Library"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Library"
            IntroPic.image = UIImage(named:"library")
            flipLabels()
        }
        
        //38.053656, -84.494347 is center of Forrer
        if ((locValue.latitude >= ForrBottomLeftCoordinate.latitude && locValue.longitude >= ForrBottomLeftCoordinate.longitude) && (locValue.latitude <= ForrTopRightCoordinate.latitude && locValue.longitude <= ForrTopRightCoordinate.longitude)){
            GPSTest.text = "Forrer/Campus Center"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Forrer/Campus Center"
            IntroPic.image = UIImage(named:"forrer")
            flipLabels()
        }
        
        //38.052988, -84.493248 is center Beck
        if ((locValue.latitude >= beckBottomLeftCoordinate.latitude && locValue.longitude >= beckBottomLeftCoordinate.longitude) && (locValue.latitude <= beckTopRightCoordinate.latitude && locValue.longitude <= beckTopRightCoordinate.longitude)){
            GPSTest.text = "Beck Center"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Beck Center"
            IntroPic.image = UIImage(named:"beck")
            flipLabels()
        }
        
        //38.050863, -84.493511 is center cowgill
        if ((locValue.latitude >= cowBottomLeftCoordinate.latitude && locValue.longitude >= cowBottomLeftCoordinate.longitude) && (locValue.latitude <= cowTopRightCoordinate.latitude && locValue.longitude <= cowTopRightCoordinate.longitude)){
            GPSTest.text = "Cowgill"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Cowgill"
            IntroPic.image = UIImage(named:"cowgill")
            flipLabels()
        }
        
        //38.052789, -84.492220 is center Shearer
        if ((locValue.latitude >= artBottomLeftCoordinate.latitude && locValue.longitude >= artBottomLeftCoordinate.longitude) && (locValue.latitude <= artTopRightCoordinate.latitude && locValue.longitude <= artTopRightCoordinate.longitude)){
            GPSTest.text = "Shearer Art Building"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Shearer Art Building"
            IntroPic.image = UIImage(named:"shearer")
            flipLabels()
        }
        
        //38.050267, -84.492527 is center Hall Field
        if ((locValue.latitude >= hallBottomLeftCoordinate.latitude && locValue.longitude >= hallBottomLeftCoordinate.longitude) && (locValue.latitude <= hallTopRightCoordinate.latitude && locValue.longitude <= hallTopRightCoordinate.longitude)){
            GPSTest.text = "Hall Field"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Hall Field"
            IntroPic.image = UIImage(named:"hall")
            flipLabels()
        }
        
        //38.050034, -84.495653 is center Carnegie
        if ((locValue.latitude >= CarnBottomLeftCoordinate.latitude && locValue.longitude >= CarnBottomLeftCoordinate.longitude) && (locValue.latitude <= CarnTopRightCoordinate.latitude && locValue.longitude <= CarnTopRightCoordinate.longitude)){
            GPSTest.text = "Carnegie Center"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Carnegie Center"
            IntroPic.image = UIImage(named:"carnegie")
            flipLabels()
        }
        
        //38.051756, -84.492307 is center Little Theatre
        if ((locValue.latitude >= LittleBottomLeftCoordinate.latitude && locValue.longitude >= LittleBottomLeftCoordinate.longitude) && (locValue.latitude <= LittleTopRightCoordinate.latitude && locValue.longitude <= LittleTopRightCoordinate.longitude)){
            GPSTest.text = "Lucille C. Little Theater"
            GPSTest.font = GPSTest.font.fontWithSize(18)
            passTest = "Lucille C. Little Theater"
            IntroPic.image = UIImage(named:"little")
            flipLabels()
        }
        
        //38.051262, -84.492408 is center Hazelrigg
        if ((locValue.latitude >= HazelBottomLeftCoordinate.latitude && locValue.longitude >= HazelBottomLeftCoordinate.longitude) && (locValue.latitude <= HazelTopRightCoordinate.latitude && locValue.longitude <= HazelTopRightCoordinate.longitude)){
            GPSTest.text = "Hazelrigg"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Hazelrigg"
            IntroPic.image = UIImage(named:"hazelrigg")
            flipLabels()
        }
        
        //38.054119, -84.493205 is center Alumni
        if ((locValue.latitude >= AlumBottomLeftCoordinate.latitude && locValue.longitude >= AlumBottomLeftCoordinate.longitude) && (locValue.latitude <= AlumTopRightCoordinate.latitude && locValue.longitude <= AlumTopRightCoordinate.longitude)){
            GPSTest.text = "Alumni and Development Building"
            GPSTest.font = GPSTest.font.fontWithSize(18)
            passTest = "Alumni and Development Building"
            IntroPic.image = UIImage(named:"415")
            flipLabels()
        }
        
        //38.055334, -84.493618 is center Poole
        if ((locValue.latitude >= PooleBottomLeftCoordinate.latitude && locValue.longitude >= PooleBottomLeftCoordinate.longitude) && (locValue.latitude <= PooleTopRightCoordinate.latitude && locValue.longitude <= PooleTopRightCoordinate.longitude)){
            GPSTest.text = "Poole Hall"
            GPSTest.font = GPSTest.font.fontWithSize(25)
            passTest = "Poole Hall"
            IntroPic.image = UIImage(named:"poole")
            flipLabels()
        }
       
        */
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "Template") {
            let svc = segue.destinationViewController as! LocationTemplateViewController;
            
            svc.namePass = GPSTest.text
            
        }
    }
    
    @IBAction func unwindToMainMenu(sender: UIStoryboardSegue)
    {
        _ = sender.sourceViewController
        // Pull any data from the view controller which initiated the unwind segue.
    }
    
    @IBAction func hideRightBar(sender: UIStoryboardSegue)
    {
        RightMenu.hidden = true
    }
    @IBAction func hideLeftBar(sender: UIStoryboardSegue)
    {
        LeftMenu.hidden = true
    }
    
    
    
    //MARK: Properties
    
    @IBOutlet weak var GPSTest: UILabel!
    @IBOutlet weak var MoreInfo: UIButton!
    @IBOutlet weak var locationValues: UILabel!
    @IBOutlet weak var IntroPic: UIImageView!
    @IBOutlet weak var helpLabel: UILabel!
    @IBOutlet weak var helpMeButton: UIButton!
    @IBOutlet weak var DebugCoor: UILabel!
    @IBOutlet weak var FMILabel: UILabel!
    @IBOutlet weak var LeftMenu: UIView!
    @IBOutlet weak var MenuButton: UIButton!
    @IBOutlet weak var RightMenu: UIView!
    @IBOutlet weak var GoToSetting: UIButton!
    @IBOutlet weak var SettingButton: UIButton!
    @IBOutlet weak var CameraButton: UIButton!
    @IBOutlet weak var PicInfo: UILabel!
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
