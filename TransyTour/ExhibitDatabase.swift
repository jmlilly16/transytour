//
//  ExhibitDatabase.swift
//  TransyTour
//
//  Created by Mason Lilly on 4/13/16.
//  Copyright © 2016 Skyler Moran and Mason Lilly. All rights reserved.
//

//  This file supplies the functions and class definitions for working with the Exhibit List.
//  In particular, it defines the Exhibit and ExhibitData objects, used to store information about the individual exhibits
//  It also defines functions to generate URLs to access the various files stored on the app's server.

import Foundation
import CoreLocation
import UIKit

//Base URL to access the app's server
let BaseURL = NSURL(string: "http://www.cs.transy.edu/TransyTour/")

//An ExhibitData object represents either an image or a narration associated with an exhibit.
//The NSData member stores the raw data for either the image or sound file, after it has been downloaded from the server
//  -The data member always starts out as nil, and then gets filled in later during network callback function
//The String member hold either an image's caption or the written-out text of a narration.
//The Boolean member is a flag so that functions can mark whether or not someone has already tried to download the given resource (to avoid unnecessary download requests every time an exhibit is opened)
class ExhibitData
{
    let text: String
    var tried = false
    var data: NSData?
    init(text: String = "")
    {
        self.text = text
    }
}

//An Exhibit object represents one exhibit of the Tour and holds all information associated with it.
//An exhibit's shortname is an abbreviated name that is used to identify the exhibit internally
//  -it is also used to generate URLs to the exhibit's resources, and thus must match the name of the exhibit's folder on the server
//  -(e.g. the Brown Science Center exhibit's shortname is bsc, and its resources are in www.cs.transy.edu/TransyTour/exhibits/bsc
//An exhibit's fullname is the name shown to the user as they view the exhibit.
//The BL/TRCoord objects represent the geographical bottom-left and top-right coordinates of the exhibit's active area
//  (typically the boundaries of the building)
//  -For historical or non-geographical exhibits, these are left nil
//The facts and images arrays hold the content for the exhibit
//  -When the Exhibit object is first created, these are empty
//  -They get filled in later as the result of the parse(fact/image)file functions
class Exhibit
{
    let shortname: String
    let fullname: String
    let BLCoord: CLLocationCoordinate2D?
    let TRCoord: CLLocationCoordinate2D?
    var facts = [ExhibitData]()
    var images = [ExhibitData]()
    
    init(shortname: String, fullname: String, BLCoord: CLLocationCoordinate2D? = nil, TRCoord: CLLocationCoordinate2D? = nil)
    {
        self.shortname = shortname
        self.fullname = fullname
        self.BLCoord = BLCoord
        self.TRCoord = TRCoord
        self.images.append(ExhibitData())   //Put in a dummy image object so the cover photo can be loaded
    }
    
    //Performs a simple boundary check on the passed-in location and Exhibit's corner locations.
    //Note that for non-geographical exhibits (those without coordinates) this function will always return false
    func containsLocation(loc: CLLocationCoordinate2D) -> Bool
    {
        if let BL = BLCoord, TR = TRCoord
        {
            return BL.latitude <= loc.latitude && BL.longitude <= loc.longitude && TR.latitude >= loc.latitude && TR.longitude >= loc.longitude
        }
        else
        {
            return false
        }
    }
    
    //Attempts to download all the resources associated with this exhibit.
    //For each fact and image defined for the exhibit, it makes an HTTP request for the image or sound file associated with it on the server
    //Note that this function should only be called after a successful call to both parseFactFile and parseCaptionFile
    func loadAllData()
    {
        for (index,fact) in facts.enumerate()
        {
            guard fact.tried == false else {continue}
            let URL = createNarrationURL(self.shortname,index: index)
            getDataFromURL(URL)
            {(data,response,error) in
                dispatch_async(dispatch_get_main_queue())
                {()->Void in
                    if error != nil
                    {
                        fact.tried = true
                    }
                    else if let data = data
                    {
                        fact.data = data
                    }
                }
            }
        }
        for (index,image) in images.enumerate()
        {
            let URL = createImageURL(self.shortname,index: index)
            guard image.tried == false else {continue}
            getDataFromURL(URL)
            {(data,response,error) in
                dispatch_async(dispatch_get_main_queue())
                {()->Void in
                    if error != nil
                    {
                        image.tried = true
                    }
                    else if let data = data
                    {
                        image.data = data
                    }
                }
            }
        }
    }
    
    func parseFactFile(text: String)
    {
        guard text.characters.count > 0 else {return}
        var facts = text.componentsSeparatedByString("\n")
        if facts.last == ""
        {
            facts.removeLast()
        }
        self.facts = [ExhibitData]()
        for line in facts
        {
            self.facts.append(ExhibitData(text: line))
        }    }
    
    func parseCaptionFile(text: String)
    {
        guard text.characters.count > 0 else {return}
        var captions = text.componentsSeparatedByString("\n")
        if captions.last == ""
        {
            captions.removeLast()
        }
        self.images = [ExhibitData]()
        for line in captions
        {
            self.images.append(ExhibitData(text: line))
        }
    }
}

var exhibits: [Exhibit]?

func parseExhibitFile(source: String) -> [Exhibit]
{
    let blocks = source.componentsSeparatedByString("\n\n")
    var result = [Exhibit]()
    for block in blocks
    {
        var lines = block.componentsSeparatedByString("\n")
        if lines.last == ""
        {
            lines.removeLast()
        }
        guard lines.count == 2 || lines.count == 4 else {continue}
        let shortname = lines[0]
        let fullname = lines[1]
        var blcline: [String]?
        var trcline: [String]?
        if lines.count==4
        {
            blcline = lines[2].componentsSeparatedByString(",")
            trcline = lines[3].componentsSeparatedByString(",")
        }
        else
        {
            blcline = nil
            trcline = nil
        }
        if let blcline = blcline, trcline = trcline, blclat = Double(blcline[0]), blclong = Double(blcline[1]), trclat = Double(trcline[0]), trclong = Double(trcline[1])
        {
            result.append(Exhibit(shortname: shortname, fullname: fullname, BLCoord: CLLocationCoordinate2D(latitude: blclat, longitude: blclong), TRCoord: CLLocationCoordinate2D(latitude: trclat, longitude: trclong)))
        }
        else
        {
            result.append(Exhibit(shortname: shortname, fullname: fullname))
        }
    }
    return result
}

func getDataFromURL(url:NSURL, completion: ((data: NSData?, response: NSURLResponse?, error: NSError? ) -> Void))
{
    NSURLSession.sharedSession().dataTaskWithURL(url)
    {
        (data, response, error) in
            completion(data: data, response: response, error: error)
    }.resume()
}

func displayCoverImage(exhibit: Exhibit, display: UIImageView)
{
    if let imagedata = exhibit.images[0].data
    {
        display.image = UIImage(data: imagedata)
    }
    else if !exhibit.images[0].tried
    {
        let URL = createIntroPicURL(exhibit.shortname)
        getDataFromURL(URL)
        {(data,response,error) in
            dispatch_async(dispatch_get_main_queue())
            {()->Void in
                if error != nil
                {
                    //do error handling things here
                    exhibit.images[0].tried = true
                }
                guard let data = data where error == nil else {return}
                exhibit.images[0].data = data
                let tempImage = UIImage(data: data)
                display.image = tempImage
            }
        }
    }
}

func createExhibitListURL() -> NSURL
{
    return NSURL(string: "exhibitList.txt", relativeToURL: BaseURL)!
}

func createNarrationURL(shortname: String, index: Int) -> NSURL
{
    return NSURL(string: "exhibits/\(shortname)/sounds/narration\(index).mp3", relativeToURL: BaseURL)!
}

func createImageURL(shortname: String, index: Int) -> NSURL
{
    return NSURL(string: "exhibits/\(shortname)/images/image\(index).jpg", relativeToURL: BaseURL)!
}

func createIntroPicURL(name: String) -> NSURL
{
    return createImageURL(name,index: 0)
}

func createFactListURL(shortname: String) -> NSURL
{
    return NSURL(string: "exhibits/\(shortname)/facts.txt", relativeToURL: BaseURL)!
}

func createCaptionListURL(shortname: String) -> NSURL
{
    return NSURL(string: "exhibits/\(shortname)/captions.txt", relativeToURL: BaseURL)!
}