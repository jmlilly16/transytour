//
//  PicInfoViewController.swift
//  TransyTour
//
//  Created by Skyler Moran on 3/21/16.
//  Copyright © 2016 Skyler Moran and Mason Lilly. All rights reserved.
//

import UIKit

class PicInfoViewController: UIViewController {
    
    var imagePass: UIImage?
    var capPass: String?
    var testCap: String?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        MorePic.image = imagePass
        LongCap.text = testCap
        MorePic.contentMode = .ScaleAspectFit
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //Mark: Properties
    @IBOutlet weak var MorePic: UIImageView!
    @IBOutlet weak var LongCap: UILabel!
    @IBOutlet weak var DateTemp: UILabel!
    
    
    /*
    // MARK: - Navigation
    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
