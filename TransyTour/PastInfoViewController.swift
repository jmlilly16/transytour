//
//  PastInfoViewController.swift
//  TransyTour
//
//  Created by Skyler Moran on 3/24/16.
//  Copyright © 2016 Skyler Moran and Mason Lilly. All rights reserved.
//

import UIKit


class PastInfoViewController: UIViewController {
    
    var pastName: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        findDetails()
    }
    
    @IBAction func unwindToPastInfo(sender: UIStoryboardSegue)
    {
        _ = sender.sourceViewController
    }
    
    
    
    func findDetails(){
        if (pastName == "College of the Bible Building"){
            BuildName.text = pastName
            BuildBuilt.text = "1895"
            BuildDest.text = "1960"
            BuildAbout.text = "This Victorian-style building was erected as part of Kentucky University, as Transylvania was then called. It overlooked Morrison Hall. The building was used for classrooms until it was replaced by the Haupt Humanities Building."
        }
        if (pastName == "Ewing Hall"){
            BuildName.text = pastName
            BuildBuilt.text = "1914"
            BuildDest.text = "Early 1960's"
            BuildAbout.text = "Ewing Hall served as a men’s dormitory and dining hall until when the Clay and Davis Residence Halls were constructed west of Broadway. in 1945, it was converted to house married veterans. Ewing Hall was demolished to make room for Mitchell Fine Arts Center."
            SeePics.hidden = true
        }
        if (pastName == "McAlister Gymnasium"){
            BuildName.text = pastName
            BuildBuilt.text = "1957"
            BuildDest.text = "2002"
            BuildAbout.text = "McAlister Auditorium was the home of many Pioneer indoor athletic teams and was often used for commencement ceremonies.  It was the home gym for Coach C.M. Newton (National Champ, Olympic Gold Medalist, Basketball Hall of fame) who also recruited Transy’s first Black athlete. It was replaced by the Clive M. Beck Athletic and Recreation Center."
        }
        if (pastName == "Craig/Logan Halls"){
            BuildName.text = pastName
            BuildBuilt.text = "1880's"
            BuildDest.text = "1914"
            BuildAbout.text = "These two buildings were located directly behind Morrison Hall.  They were destroyed to make room for Ewing Hall."
        }
        if (pastName == "Ella Jones/Lyons-Hamilton Halls"){
            BuildName.text = pastName
            BuildBuilt.text = "1908"
            BuildDest.text = "1962"
            BuildAbout.text = "These buildings were also known as East Hall.  Originally, they were apart of Hamilton College. Hamilton College was an all female college that later merged into Transylvania University. It was razed to build Forrer Hall."
            SeePics.hidden = true
        }
        if (pastName == "Henry Clay/Jefferson Davis Halls"){
            BuildName.text = pastName
            BuildBuilt.text = "1962"
            BuildDest.text = "2015"
            BuildAbout.text = "These two buildings held strictly men.  Clay was reserved for freshmen males, where Davis was reserved for the fraternities. They included basements where fraternity chapter rooms were located.  Encompassing “Back Circle,” Clay and Davis became an important social hub for student life until their demolition."
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "PicInfo") {
            let svc = segue.destinationViewController as! HistoryPicsViewController;
            
            svc.BuildName = BuildName.text
            
        }
        
        
    }
    
    
    
    //MARK: Properties
    
    @IBOutlet weak var BuildName: UILabel!
    @IBOutlet weak var BuildBuilt: UILabel!
    @IBOutlet weak var BuildDest: UILabel!
    @IBOutlet weak var BuildAbout: UILabel!
    @IBOutlet weak var BuildPic: UIImageView!
    @IBOutlet weak var SeePics: UIButton!
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
