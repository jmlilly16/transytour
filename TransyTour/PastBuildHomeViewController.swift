//
//  PastBuildHomeViewController.swift
//  TransyTour
//
//  Created by Skyler Moran on 3/24/16.
//  Copyright © 2016 Skyler Moran and Mason Lilly. All rights reserved.
//

import UIKit

class PastBuildHomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToPastBuild(sender: UIStoryboardSegue)
    {
        _ = sender.sourceViewController
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
