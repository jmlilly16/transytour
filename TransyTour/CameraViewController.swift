//
//  CameraViewController.swift
//  TransyTour
//
//  Created by Skyler Moran on 3/21/16.
//  Copyright © 2016 Skyler Moran and Mason Lilly. All rights reserved.
//http://www.techotopia.com/index.php/An_Example_Swift_iOS_8_iPhone_Camera_Application
//

import UIKit
import MobileCoreServices
import AVFoundation

class CameraViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    var newMedia: Bool?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func useCamera(sender: AnyObject) {
        
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.Camera) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerControllerSourceType.Camera
                imagePicker.mediaTypes = [kUTTypeImage as NSString as String]
                imagePicker.allowsEditing = false
                
                self.presentViewController(imagePicker, animated: true, completion: nil)
                newMedia = true
            imageView.contentMode = .ScaleAspectFit
        }
        else{
            imageView.image = UIImage(named:"Haupt")
            imageView.contentMode = .ScaleAspectFit
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        imageView.image = image
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func useCameraRoll(sender: AnyObject) {
        
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.SavedPhotosAlbum) {
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerControllerSourceType.PhotoLibrary
                imagePicker.mediaTypes = [kUTTypeImage as NSString as String]
                imagePicker.allowsEditing = false
                self.presentViewController(imagePicker, animated: true,
                    completion: nil)
                newMedia = false
        }
    }
    
    @IBAction func savePicture(sender: AnyObject){
        
        if(imageView.image != nil){
        UIImageWriteToSavedPhotosAlbum(imageView.image!, nil, nil, nil)
        
        let saveAlert = UIAlertView()
        saveAlert.title = "Picture Saved!"
        saveAlert.message = "You picture has been successfully saved!"
        saveAlert.addButtonWithTitle("OK")
        saveAlert.delegate = self
        saveAlert.show()
        }
        else{
            let errorAlert = UIAlertView()
            errorAlert.title = "Picture Not Saved!"
            errorAlert.message = "You do not have an image availible to save!"
            errorAlert.addButtonWithTitle("OK")
            errorAlert.delegate = self
            errorAlert.show()
        }
    }
    

    
    // MARK: - Navigation
    
    //MARK: Properties
    @IBOutlet weak var CameraView: UIImageView!
    @IBOutlet weak var CamSnap: UIButton!
    @IBOutlet weak var CamRoll: UIButton!
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var SavePic: UIButton!
    
    
/*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
