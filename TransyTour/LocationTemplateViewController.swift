//
//  LocationTemplateViewController.swift
//  TransyTour
//
//  Created by Skyler Moran on 2/22/16.
//  Copyright © 2016 Skyler Moran and Mason Lilly. All rights reserved.
//

import UIKit
import AVFoundation

class LocationTemplateViewController: UIViewController, AVAudioPlayerDelegate {
    
    var namePass:String!
    var longPass:Double!
    var latPass:Double!
    var shownExhibit:Exhibit?

    var currentFactIndex: Int = 0
    var currentImageIndex: Int = 0
    
    var audioPlayer = AVAudioPlayer()
    
    //Music Files
    let testSoundURL =  NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("soar-noisy_oyster", ofType: "mp3")!)
    let rickSoundURL =  NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("RickQuote", ofType: "mp3")!)
    let Quiet =  NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("point1sec", ofType: "mp3")!)

    override func viewDidLoad() {
        super.viewDidLoad()
        LocationTemp.text = namePass
        
        //set shownExhibit based on the name passed in
        if let exhibits = exhibits
        {
            for exhibit in exhibits
            {
                if exhibit.fullname == LocationTemp.text
                {
                    shownExhibit = exhibit
                    break
                }
            }
        }
        
        self.fillDetails()
        ImageTemp.contentMode = .ScaleAspectFit
        
        
        do {
            self.audioPlayer = try AVAudioPlayer(contentsOfURL: Quiet, fileTypeHint: "mp3")
            
            self.audioPlayer.prepareToPlay()
            self.audioPlayer.play()
            
        } catch {
            print("Error")
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        if(audioPlayer.playing){
            audioPlayer.stop()
        }
         //Stops music once you leave controller
        
    }

    @IBAction func unwindToLocTemp(sender: UIStoryboardSegue)
    {
        _ = sender.sourceViewController
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillDetails()
    {
        //Find the exhibit with the matching name
        //Load all the images, captions, facts, and narrations into arrays into the exhibit object
        //Put the 0-index content into the UI

        if let exhibit = shownExhibit
        {
            exhibit.loadAllData()
            displayCoverImage(exhibit, display: ImageTemp)
            displayTextOnLabel(FactTemp, text: exhibit.facts[0].text)
            currentImageIndex = 0
            currentFactIndex = 0
            Caption.hidden = false
            MorePicButton.hidden = false
        }

    }
    
//Button Action to show next fact for location
    @IBAction func showNextFact(sender: AnyObject)
    {
        if(audioPlayer.playing)
        {
            audioPlayer.stop()
        }
        guard let exhibit = shownExhibit else {return}
        currentFactIndex += 1
        if currentFactIndex == exhibit.facts.count
        {
            currentFactIndex = 0
        }
        displayTextOnLabel(FactTemp, text: exhibit.facts[currentFactIndex].text)

    }
    
    @IBAction func showLastFact(sender: AnyObject)
    {
        if(audioPlayer.playing)
        {
            audioPlayer.stop()
        }
        
        guard let exhibit = shownExhibit else {return}
        if currentFactIndex == 0
        {
            currentFactIndex = exhibit.facts.count
        }
        currentFactIndex -= 1
        displayTextOnLabel(FactTemp, text: exhibit.facts[currentFactIndex].text)
    }
    
    @IBAction func showNextImage(sender: AnyObject)
    {
        guard let exhibit = shownExhibit else {return}
        currentImageIndex += 1
        if currentImageIndex == exhibit.images.count
        {
            currentImageIndex = 0
        }
        if let imagedata = exhibit.images[currentImageIndex].data
        {
            ImageTemp.image = UIImage(data: imagedata)
        }
    }
    
    @IBAction func showLastImage(sender: AnyObject)
    {
        guard let exhibit = shownExhibit else {return}
        if currentImageIndex == 0
        {
            currentImageIndex = exhibit.images.count
        }
        currentImageIndex -= 1
        if let imagedata = exhibit.images[currentImageIndex].data
        {
            ImageTemp.image = UIImage(data: imagedata)
        }
    }
    
    func playMySound()
    {
        do
        {
            self.audioPlayer = try AVAudioPlayer(contentsOfURL: testSoundURL, fileTypeHint: "mp3")
            if let exhibit = shownExhibit
            {
                if let sounddata = exhibit.facts[currentFactIndex].data
                {
                    self.audioPlayer = try AVAudioPlayer(data: sounddata)
                }
            }
            self.audioPlayer.prepareToPlay()
            self.audioPlayer.play()
            
        }
        catch
        {
            print("Error")
        }
    }
    
    @IBAction func readFact()
    {
            playMySound()
    }

    //Pass in Captions for the more info controller
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
    {
        if (segue.identifier == "MoreInfo") {
            let svc = segue.destinationViewController as! PicInfoViewController;
            if(ImageTemp.image != nil)
            {
                svc.imagePass = ImageTemp.image!
                svc.testCap = shownExhibit!.images[currentImageIndex].text
                //svc.capPass = Caption.text
            }
        }
    }
    
    func displayTextOnLabel(label: UILabel, text: String)
    {
        let calculatedFontSize: Double = Double(5000 / Double(text.characters.count))
        let finalFontSize: CGFloat = CGFloat(min(calculatedFontSize,16.0))
        label.font = label.font.fontWithSize(finalFontSize)
        label.text = text
    }
    
    //Mark: Properties
    @IBOutlet weak var LocationTemp: UILabel!
    @IBOutlet weak var FactTemp: UILabel!
    @IBOutlet weak var ImageTemp: UIImageView!
    @IBOutlet weak var Caption: UILabel!
    @IBOutlet weak var ReadButton: UIButton!
    @IBOutlet weak var MorePicButton: UIButton!
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
