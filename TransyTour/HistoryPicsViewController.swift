//
//  HistoryPicsViewController.swift
//  TransyTour
//
//  Created by Skyler Moran on 3/28/16.
//  Copyright © 2016 Skyler Moran and Mason Lilly. All rights reserved.
//

import UIKit

class HistoryPicsViewController: UIViewController {
    
    var BuildName: String!
    var currentImageIndex = 0
    
    var CDImage: [UIImage] = [
        UIImage(named: "Davis2015")!,
        UIImage(named: "BackCircle1969")!
    ]
    let CDCaption: [String] = ["Davis. Feb 2015","Back Circle featuring Clay Hall. 1969."]
    
    var BibleImage: [UIImage] = [
        UIImage(named: "collegebible")!,
        UIImage(named: "collegebible2")!
    ]
    let BibleCaption: [String] = ["College of the Bible Building, 1904","Another view of College of the Bible Building, Date Unknown"]
    
    var GymImage: [UIImage] = [
        UIImage(named: "gym1")!,
        UIImage(named: "gym2")!
    ]
    let GymCaption: [String] = ["Court Side in McAlister Gymnasium, Date Unknown","Commencement Ceromony, Date Unknown"]
    
    var LCImage: [UIImage] = [
        UIImage(named: "logan1")!,
        UIImage(named: "logan2")!
    ]
    let LCCaption: [String] = ["Postcard displaying Logan and Craig Hall, Date Unknown","Back of Logan and Craig Hall postcard, Date Unknown"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        PicView.transform = CGAffineTransformMakeRotation(3.14/2);
        HistImage.contentMode = .ScaleAspectFit
        
        if (BuildName == "Henry Clay/Jefferson Davis Halls"){
            HistImage.image = CDImage[currentImageIndex]
            Caption.text = CDCaption[currentImageIndex]
        }
        
        if (BuildName == "College of the Bible Building"){
            HistImage.image = BibleImage[currentImageIndex]
            Caption.text = BibleCaption[currentImageIndex]
        }
        
        if (BuildName == "McAlister Gymnasium"){
            HistImage.image = GymImage[currentImageIndex]
            Caption.text = GymCaption[currentImageIndex]
        }
        
        if (BuildName == "Craig/Logan Halls"){
            HistImage.image = LCImage[currentImageIndex]
            Caption.text = LCCaption[currentImageIndex]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    
    @IBAction func showNextImage(sender: AnyObject){
        if (BuildName == "Henry Clay/Jefferson Davis Halls"){
            currentImageIndex += 1
            
            if (currentImageIndex == CDImage.count){
                currentImageIndex = 0
            }
            HistImage.image = CDImage[currentImageIndex]
            Caption.text = CDCaption[currentImageIndex]
        }
        
        if (BuildName == "College of the Bible Building"){
            currentImageIndex += 1
            
            if (currentImageIndex == BibleImage.count){
                currentImageIndex = 0
            }
            HistImage.image = BibleImage[currentImageIndex]
            Caption.text = BibleCaption[currentImageIndex]
        }
        
        if (BuildName == "McAlister Gymnasium"){
            currentImageIndex += 1
            
            if (currentImageIndex == GymImage.count){
                currentImageIndex = 0
            }
            HistImage.image = GymImage[currentImageIndex]
            Caption.text = GymCaption[currentImageIndex]
        }
        
        if (BuildName == "Craig/Logan Halls"){
            currentImageIndex += 1
            
            if (currentImageIndex == LCImage.count){
                currentImageIndex = 0
            }
            HistImage.image = LCImage[currentImageIndex]
            Caption.text = LCCaption[currentImageIndex]
        }
        
        
    }
    
    @IBAction func showLastImage(sender: AnyObject){
        if (BuildName == "Henry Clay/Jefferson Davis Halls"){
            
            if (currentImageIndex == 0){
                currentImageIndex = CDImage.count
            }
            currentImageIndex -= 1
            HistImage.image = CDImage[currentImageIndex]
            Caption.text = CDCaption[currentImageIndex]
        }
        
        if (BuildName == "College of the Bible Building"){
            
            if (currentImageIndex == 0){
                currentImageIndex = BibleImage.count
            }
            currentImageIndex -= 1
            HistImage.image = BibleImage[currentImageIndex]
            Caption.text = BibleCaption[currentImageIndex]
        }
        
        if (BuildName == "McAlister Gymnasium"){
            
            if (currentImageIndex == 0){
                currentImageIndex = GymImage.count
            }
            currentImageIndex -= 1
            HistImage.image = GymImage[currentImageIndex]
            Caption.text = GymCaption[currentImageIndex]
        }
        
        if (BuildName == "Craig/Logan Halls"){
            
            if (currentImageIndex == 0){
                currentImageIndex = LCImage.count
            }
            currentImageIndex -= 1
            HistImage.image = LCImage[currentImageIndex]
            Caption.text = LCCaption[currentImageIndex]
        }
        
    }
    
    //Mark: Properties

    @IBOutlet var PicView: UIView!
    @IBOutlet weak var HistImage: UIImageView!
    @IBOutlet weak var LastImage: UIButton!
    @IBOutlet weak var NextImage: UIButton!
    @IBOutlet weak var Caption: UILabel!
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
