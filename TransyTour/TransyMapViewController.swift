//
//  TransyMapViewController.swift
//  TransyTour
//
//  Created by Skyler Moran on 2/9/16.
//  Copyright © 2016 Skyler Moran and Mason Lilly. All rights reserved.
//

import UIKit

class TransyMapViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.transform = CGAffineTransformMakeRotation(3.14/2);
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    @IBAction func unwindToMap(sender: UIStoryboardSegue)
    {
        _ = sender.sourceViewController
        // Pull any data from the view controller which initiated the unwind segue.
    }
    
    //MARK: Properties
   
    @IBOutlet var imageView: UIView!
    @IBOutlet weak var NameList: UIButton!
    @IBOutlet weak var HomeButton: UIButton!

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
