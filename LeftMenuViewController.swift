//
//  LeftMenuViewController.swift
//  TransyTour
//
//  Created by Skyler Moran on 3/2/16.
//  Copyright © 2016 Skyler Moran. All rights reserved.
//

import UIKit

class LeftMenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        SettingMenu.hidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func toggleMenu(){
        SettingMenu.hidden = false
    }
    
    func closeMenu(animated:Bool = true){
        scrollView.setContentOffset(CGPoint(x: 218, y: 0), animated: animated)
    }
    
    func openMenu(){
        print("opening menu")
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    @IBAction func toggleMenu(sender: AnyObject) {
        toggleMenu()
    }

    //Properties
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var SettingMenu: UIView!
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
