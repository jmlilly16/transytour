//
//  GlobalMapViewController.swift
//  TransyTour
//
//  Created by Skyler Moran on 3/2/16.
//  Copyright © 2016 Skyler Moran and Mason Lilly. All rights reserved.
//
//https://www.iconfinder.com/iconsets/eightyshades 
//https://www.iconfinder.com/icons/763485/arrow_back_circle_direction_left_navigation_previous_icon#size=128
//https://www.raywenderlich.com/90971/introduction-mapkit-swift-tutorial

import UIKit
import MapKit
import Foundation
import CoreLocation

class GlobalMapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    var locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        mapView.showsUserLocation = true
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(locationManager.location!.coordinate, 500, 500)
        mapView.setRegion(coordinateRegion, animated: true)
        mapView.mapType = MKMapType.Satellite
        LoadPoints()    //Load in the exhibit points as defined by the exhibit list
        mapView.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        GPStesting.text = ("Current Latitude: \(locValue.latitude)")
        GPSTest2.text = ("Current Longitude: \(locValue.longitude)")
    }
    
    @IBAction func zoomIn(sender: AnyObject) {
        let userLocation = mapView.userLocation
        
        let region = MKCoordinateRegionMakeWithDistance(
            userLocation.location!.coordinate, 250, 250)
        
        mapView.setRegion(region, animated: true)
    }
    
    @IBAction func zoomOut(sender: AnyObject) {
        let userLocation = mapView.userLocation
        
        let region = MKCoordinateRegionMakeWithDistance(
            userLocation.location!.coordinate, 1000, 1000)
        
        mapView.setRegion(region, animated: true)
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationIdentifier = "pin"
        if annotation is MKUserLocation {
            return nil
        }
        var view = mapView.dequeueReusableAnnotationViewWithIdentifier(annotationIdentifier)
        if view == nil {
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            view?.canShowCallout = true
            view?.rightCalloutAccessoryView = UIButton(type: .DetailDisclosure)
        } else {
            view?.annotation = annotation
        }
        return view
    }
    
    var selectedAnnotation: MKPointAnnotation!
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if (control == view.rightCalloutAccessoryView) {
            selectedAnnotation = view.annotation as? MKPointAnnotation
            performSegueWithIdentifier("toLocTemp", sender: self)
        }
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let svc = segue.destinationViewController as? LocationTemplateViewController {
            svc.namePass = selectedAnnotation.title
        }
    }
    
    
    //Adds annotations to the mapView by calculating positions from exhibits in the exhibit list
    func LoadPoints()
    {
        //For each exhibit in the database, look and see if it has coordinates. if it does, make one of these annotations for it.
        guard let exhibits = exhibits else {return}
        for exhibit in exhibits
        {
            if let BLCoord = exhibit.BLCoord, TRCoord = exhibit.TRCoord //If this exhibit has coordinates...
            {   //Take the average of both coordinates and create a "center" coordinate
                let centerCoord = CLLocationCoordinate2D(latitude: (BLCoord.latitude+TRCoord.latitude) / 2, longitude: (BLCoord.longitude+TRCoord.longitude) / 2)
                let annotation = MKPointAnnotation()    //Make an annotation object
                annotation.coordinate = centerCoord //Position it at the location you just calculated
                annotation.title = exhibit.fullname //Give it the same name as the exhibit
                annotation.subtitle = "Transy"
                mapView.addAnnotation(annotation)   //Add it to the map!
            }
        }
/*
        let HauptLoc = CLLocationCoordinate2DMake(38.052216, -84.493549)
        let HHannotation = MKPointAnnotation()
        HHannotation.coordinate = HauptLoc
        HHannotation.title = "Haupt"
        HHannotation.subtitle = "Transy"
        mapView.addAnnotation(HHannotation)
        
        let MFALoc = CLLocationCoordinate2DMake(38.052320, -84.492715)
        let MFAannotation = MKPointAnnotation()
        MFAannotation.coordinate = MFALoc
        MFAannotation.title = "MFA"
        MFAannotation.subtitle = "Transy"
        mapView.addAnnotation(MFAannotation)
        
        let OMLoc = CLLocationCoordinate2DMake(38.051675, -84.493368)
        let OMannotation = MKPointAnnotation()
        OMannotation.coordinate = OMLoc
        OMannotation.title = "Old Morrison"
        OMannotation.subtitle = "Transy"
        mapView.addAnnotation(OMannotation)
        
        let BSCLoc = CLLocationCoordinate2DMake(38.050896, -84.493111)
        let BSCannotation = MKPointAnnotation()
        BSCannotation.coordinate = BSCLoc
        BSCannotation.title = "BSC"
        BSCannotation.subtitle = "Transy"
        mapView.addAnnotation(BSCannotation)
        
        let FieldLoc = CLLocationCoordinate2DMake(38.058357, -84.496967)
        let Fieldannotation = MKPointAnnotation()
        Fieldannotation.coordinate = FieldLoc
        Fieldannotation.title = "Athletic Complex"
        Fieldannotation.subtitle = "Transy"
        mapView.addAnnotation(Fieldannotation)
        
        let RosyLoc = CLLocationCoordinate2DMake(38.054659, -84.493728)
        let Rosyannotation = MKPointAnnotation()
        Rosyannotation.coordinate = RosyLoc
        Rosyannotation.title = "Rosenthal"
        Rosyannotation.subtitle = "Transy"
        mapView.addAnnotation(Rosyannotation)
        
        let AdmitLoc = CLLocationCoordinate2DMake(38.051653, -84.494214)
        let Admitannotation = MKPointAnnotation()
        Admitannotation.coordinate = AdmitLoc
        Admitannotation.title = "Admission/Jazzmans"
        Admitannotation.subtitle = "Transy"
        mapView.addAnnotation(Admitannotation)
        
        let LibLoc = CLLocationCoordinate2DMake(38.051900, -84.494356)
        let Libannotation = MKPointAnnotation()
        Libannotation.coordinate = LibLoc
        Libannotation.title = "Library"
        Libannotation.subtitle = "Transy"
        mapView.addAnnotation(Libannotation)
        
        let ForrLoc = CLLocationCoordinate2DMake(38.053656, -84.494347)
        let Forrannotation = MKPointAnnotation()
        Forrannotation.coordinate = ForrLoc
        Forrannotation.title = "Forrer/Campus Center"
        Forrannotation.subtitle = "Transy"
        mapView.addAnnotation(Forrannotation)
        
        let BeckLoc = CLLocationCoordinate2DMake(38.052988, -84.493248)
        let Beckannotation = MKPointAnnotation()
        Beckannotation.coordinate = BeckLoc
        Beckannotation.title = "Beck Center"
        Beckannotation.subtitle = "Transy"
        mapView.addAnnotation(Beckannotation)
        
        let CowLoc = CLLocationCoordinate2DMake(38.050863, -84.493511)
        let Cowannotation = MKPointAnnotation()
        Cowannotation.coordinate = CowLoc
        Cowannotation.title = "Cowgill"
        Cowannotation.subtitle = "Transy"
        mapView.addAnnotation(Cowannotation)
        
        let ArtLoc = CLLocationCoordinate2DMake(38.052789, -84.492220)
        let Artannotation = MKPointAnnotation()
        Artannotation.coordinate = ArtLoc
        Artannotation.title = "Shearer Art Building"
        Artannotation.subtitle = "Transy"
        mapView.addAnnotation(Artannotation)
        
        let HallLoc = CLLocationCoordinate2DMake(38.050267, -84.492527)
        let Hallannotation = MKPointAnnotation()
        Hallannotation.coordinate = HallLoc
        Hallannotation.title = "Hall Field"
        Hallannotation.subtitle = "Transy"
        mapView.addAnnotation(Hallannotation)
        
        let CarnLoc = CLLocationCoordinate2DMake(38.050034, -84.495653)
        let Carnannotation = MKPointAnnotation()
        Carnannotation.coordinate = CarnLoc
        Carnannotation.title = "Carnegie Center"
        Carnannotation.subtitle = "Ex-Transy"
        mapView.addAnnotation(Carnannotation)
        
        let LittleLoc = CLLocationCoordinate2DMake(38.051796, -84.492329)
        let Littleannotation = MKPointAnnotation()
        Littleannotation.coordinate = LittleLoc
        Littleannotation.title = "Lucille C. Little Theater"
        Littleannotation.subtitle = "Transy"
        mapView.addAnnotation(Littleannotation)
        
        let HazelLoc = CLLocationCoordinate2DMake(38.051262, -84.492408)
        let Hazelannotation = MKPointAnnotation()
        Hazelannotation.coordinate = HazelLoc
        Hazelannotation.title = "Hazelrigg"
        Hazelannotation.subtitle = "Transy"
        mapView.addAnnotation(Hazelannotation)
        
        let AlumLoc = CLLocationCoordinate2DMake(38.054119, -84.493205)
        let Alumannotation = MKPointAnnotation()
        Alumannotation.coordinate = AlumLoc
        Alumannotation.title = "Alumni and Development Building"
        Alumannotation.subtitle = "Transy"
        mapView.addAnnotation(Alumannotation)
        
        let PooleLoc = CLLocationCoordinate2DMake(38.055334, -84.493618)
        let Pooleannotation = MKPointAnnotation()
        Pooleannotation.coordinate = PooleLoc
        Pooleannotation.title = "Poole Hall"
        Pooleannotation.subtitle = "Transy"
        mapView.addAnnotation(Pooleannotation)
*/
    }
    

    
    // MARK: - Navigation
    
    //Properties
    
    @IBOutlet weak var GPStesting: UILabel!
    @IBOutlet weak var GPSTest2: UILabel!
    
    //Properties
    @IBOutlet weak var mapView: MKMapView!
/*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
